#!/bin/sh

sudo yum update -y
sudo amazon-linux-extras install -y php7.2
sudo amazon-linux-extras install -y nginx1.12
sudo yum install -y php-xml php-mbstring php-zip
sudo systemctl start nginx
sudo systemctl enable nginx
wget https://elasticache-downloads.s3.amazonaws.com/ClusterClient/PHP-7.2/latest-64bit
tar -zxvf latest-64bit
sudo cp amazon-elasticache-cluster-client.so /lib64/php/modules/
sudo touch /etc/php.d/30-custom.ini
sudo chown $(id -u):$(id -g) /etc/php.d/30-custom.ini
echo "post_max_size = 5G" >> /etc/php.d/30-custom.ini
echo "upload_max_filesize = 5G" >> /etc/php.d/30-custom.ini
echo "extension=amazon-elasticache-cluster-client" >> /etc/php.d/30-custom.ini
echo "max_execution_time=30000" >> /etc/php.d/30-custom.ini
sudo chown root:root /etc/php.d/30-custom.ini
sudo systemctl restart php-fpm
sudo systemctl restart nginx
wget https://wordpress.org/latest.tar.gz
tar -xzf latest.tar.gz
sudo cp -r wordpress/* /usr/share/nginx/html/
sudo chown -R apache:root /usr/share/nginx/html