# Get the ROUTE 53 zone id
ZONE="$(aws route53 --query='HostedZones[?Name==`appertly.com.`].Id' list-hosted-zones --output text)"
ZONE_ID=${ZONE#"/hostedzone/"}

# TXT record
CHALLENGE_DOMAIN="_acme-challenge.$CERTBOT_DOMAIN."

# Remove the challenge TXT record from the zone
echo $(aws route53 list-resource-record-sets --query='ResourceRecordSets[?Name==`'$CHALLENGE_DOMAIN'`]' --hosted-zone-id $ZONE_ID | jq -s '{Changes:[{Action: "DELETE", ResourceRecordSet: .[0][]}]}') > /tmp/change_batch.json

aws route53 change-resource-record-sets --hosted-zone-id $ZONE_ID --change-batch file:///tmp/change_batch.json