#!/bin/sh

mkdir -p /etc/letsencrypt/live/SUBDOMAIN.appertly.com/
mkdir -p /etc/letsencrypt/live/auth.SUBDOMAIN.appertly.com/

# check S3 for the certs
SERVER=$(aws s3api list-objects-v2 --bucket twinsisters-packer --query "Contents[?contains(Key, 'ENVIRONMENT/certificates/SUBDOMAIN.appertly.com/privkey.pem')]" | jq '. | length')
AUTH_SERVER=$(aws s3api list-objects-v2 --bucket twinsisters-packer --query "Contents[?contains(Key, 'ENVIRONMENT/certificates/auth.SUBDOMAIN.appertly.com/privkey.pem')]" | jq '. | length')

# get certificates
if [ "$SERVER" = "0" ]
then
    echo 'get a new cert'
    sudo certbot -q --manual-public-ip-logging-ok --manual-auth-hook /etc/letsencrypt/ssl-authenticator.sh --manual-cleanup-hook /etc/letsencrypt/cleanup.sh -d SUBDOMAIN.appertly.com
    until [ -f  /etc/letsencrypt/live/SUBDOMAIN.appertly.com/privkey.pem ]
    do
         sleep 5
    done
    echo "File found"
    aws s3api put-object --bucket twinsisters-packer --key ENVIRONMENT/certificates/SUBDOMAIN.appertly.com/privkey.pem --body /etc/letsencrypt/live/SUBDOMAIN.appertly.com/privkey.pem
    aws s3api put-object --bucket twinsisters-packer --key ENVIRONMENT/certificates/SUBDOMAIN.appertly.com/fullchain.pem --body /etc/letsencrypt/live/SUBDOMAIN.appertly.com/fullchain.pem
else
    echo 'get the files from s3'
    aws s3api get-object --bucket twinsisters-packer --key ENVIRONMENT/certificates/SUBDOMAIN.appertly.com/privkey.pem /etc/letsencrypt/live/SUBDOMAIN.appertly.com/privkey.pem
    aws s3api get-object --bucket twinsisters-packer --key ENVIRONMENT/certificates/SUBDOMAIN.appertly.com/fullchain.pem /etc/letsencrypt/live/SUBDOMAIN.appertly.com/fullchain.pem
fi

if [ "$AUTH_SERVER" = "0" ]
then
    echo 'get a new auth cert'
    sudo certbot -q --manual-public-ip-logging-ok --manual-auth-hook /etc/letsencrypt/ssl-authenticator.sh --manual-cleanup-hook /etc/letsencrypt/cleanup.sh -d auth.SUBDOMAIN.appertly.com
    until [ -f  /etc/letsencrypt/live/auth.SUBDOMAIN.appertly.com/privkey.pem ]
    do
         sleep 5
    done
    echo "File found"
    aws s3api put-object --bucket twinsisters-packer --key ENVIRONMENT/certificates/auth.SUBDOMAIN.appertly.com/privkey.pem --body /etc/letsencrypt/live/auth.SUBDOMAIN.appertly.com/privkey.pem
    aws s3api put-object --bucket twinsisters-packer --key ENVIRONMENT/certificates/auth.SUBDOMAIN.appertly.com/fullchain.pem --body /etc/letsencrypt/live/auth.SUBDOMAIN.appertly.com/fullchain.pem
else
    echo 'get the files from s3'
    aws s3api get-object --bucket twinsisters-packer --key ENVIRONMENT/certificates/auth.SUBDOMAIN.appertly.com/privkey.pem /etc/letsencrypt/live/auth.SUBDOMAIN.appertly.com/privkey.pem
    aws s3api get-object --bucket twinsisters-packer --key ENVIRONMENT/certificates/auth.SUBDOMAIN.appertly.com/fullchain.pem /etc/letsencrypt/live/auth.SUBDOMAIN.appertly.com/fullchain.pem
fi