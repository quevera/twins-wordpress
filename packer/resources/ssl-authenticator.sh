#!/bin/bash

# Get the ROUTE 53 zone id
ZONE="$(aws route53 --query='HostedZones[?Name==`appertly.com.`].Id' list-hosted-zones --output text)"
ZONE_ID=${ZONE#"/hostedzone/"}

# Create TXT record
CREATE_DOMAIN="_acme-challenge.$CERTBOT_DOMAIN"

CHANGE_BATCH='{"Changes": [{"Action": "CREATE", "ResourceRecordSet": {"Name":"'"$CREATE_DOMAIN"'","Type": "TXT","TTL": 300,"ResourceRecords": [{ "Value": "\"'"$CERTBOT_VALIDATION"'\""}]}}]}'

aws route53 change-resource-record-sets --hosted-zone-id $ZONE_ID --change-batch "$CHANGE_BATCH"

sleep 25