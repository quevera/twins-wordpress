#!/bin/sh

# download our premium plugins from S3

sudo aws s3 cp s3://twinsisters-packer/WordpressPlugins . --recursive

# get the jdd-svg-support plugin

sudo wget -O jdd-svg-support-master.zip https://github.com/jdelia/jdd-svg-support/archive/master.zip


# Install the plugins and activate

sudo wp plugin install --path=/usr/share/nginx/html \
    adminimize --activate \
    all-in-one-wp-security-and-firewall --activate \
    all-in-one-wp-migration-unlimited-extension.zip --activate \
    breadcrumb-navxt --activate \
    cac-login.zip --activate \
    contact-form-7 --activate \
    duplicate-page --activate \
    events-made-easy --activate \
    gravityforms_2.4.17.17.zip --activate \
    jdd-svg-support-master.zip --activate \
    jetpack --activate \
    learnpress --activate \
    learnpress-certificates.zip --activate \
    learnpress-content-drip.zip --activate \
    learnpress-course-review --activate \
    learnpress-fill-in-blank --activate \
    learnpress-import-export --activate \
    learnpress-random-quiz.zip --activate \
    learnpress-sorting-choice.zip --activate \
    learnpress-wishlist --activate \
    learnpress-woo-payment.zip --activate \
    meks-simple-flickr-widget --activate \
    nav-menu-roles --activate \
    wp-nested-pages --activate \
    redux-framework --activate \
    restrict-user-access --activate \
    supportcandy --activate \
    theme-my-login --activate \
    twinsisters-approve-master.zip --activate \
    user-role-editor --activate \
    woocommerce --activate \
    woocommerce-admin --activate \
    woocommerce-gateway-paypal-express-checkout --activate \
    woocommerce-services --activate \
    wp-file-manager --activate \
    wp-ses --activate \
    wp-retina-2x --activate \
    wp-webinarsystem --activate \
    wpsc-gravity-forms-v1.0.3.zip --activate \
    yith-woocommerce-quick-view --activate \
    yith-woocommerce-wishlist --activate