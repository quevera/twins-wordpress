data "aws_ami" "wordpress" {
  most_recent      = true

  filter {
    name   = "name"
    values = ["twinsisters-wordpress-*"]
  }

  filter {
    name   = "virtualization-type"
    values = ["hvm"]
  }

  owners = ["176696157906"]
}