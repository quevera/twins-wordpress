variable "vpc_id" {
  type = string
  default = "vpc-f8c0c49d"
}

variable "subdomain" {
  type = string
  default = "test"
}

variable "region" {
  type = string
  default = "us-east-1"
}

# Variables passed in through the command line
variable "environment" {
  type = string
}