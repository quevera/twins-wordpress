resource "aws_lb" "wordpress" {
  name               = "wordpress-nlb-tf"
  internal           = false
  load_balancer_type = "network"
  subnets            = ["subnet-6decbb1a","subnet-9b089ab0"]

  tags = {
    Name = "Terraform Test"
  }
}

resource "aws_lb_target_group" "wordpress" {
  name     = "tf-wordpress-nlb-tg"
  port     = 443
  protocol = "TCP"
  vpc_id   = var.vpc_id

  tags = {
    Name = "Terraform Test"
  }
}

resource "aws_lb_listener" "wordpress" {
  load_balancer_arn = aws_lb.wordpress.arn
  port              = "443"
  protocol          = "TCP"

  default_action {
    type             = "forward"
    target_group_arn = aws_lb_target_group.wordpress.arn
  }
}

resource "aws_lb_target_group_attachment" "wordpress" {
  target_group_arn = aws_lb_target_group.wordpress.arn
  target_id        = aws_spot_instance_request.wordpress.spot_instance_id
  port             = 443
}