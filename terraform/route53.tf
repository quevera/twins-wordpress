data "aws_route53_zone" "wordpress" {
  name         = "appertly.com."
}

resource "aws_route53_record" "subdomain" {
  zone_id = data.aws_route53_zone.wordpress.zone_id
  name    = "${var.subdomain}.${data.aws_route53_zone.wordpress.name}"
  type    = "A"
  alias {
    name                   = aws_lb.wordpress.dns_name
    zone_id                = aws_lb.wordpress.zone_id
    evaluate_target_health = true
  }
}

resource "aws_route53_record" "auth_subdomain" {
  zone_id = data.aws_route53_zone.wordpress.zone_id
  name    = "auth.${var.subdomain}.${data.aws_route53_zone.wordpress.name}"
  type    = "A"
  alias {
    name                   = aws_lb.wordpress.dns_name
    zone_id                = aws_lb.wordpress.zone_id
    evaluate_target_health = true
  }
}