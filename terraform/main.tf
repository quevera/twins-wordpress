provider "aws" {
  region  = "us-east-1"
}

resource "aws_spot_instance_request" "wordpress" {
  ami                   = data.aws_ami.wordpress.id
  instance_type         = "t3.medium"
  key_name              = "terraform"
  iam_instance_profile  = "ec2InstanceRole"
  wait_for_fulfillment = true
  spot_type = "one-time"

  tags = {
    Name = "twinsisters-wordpress-test"
  }
  vpc_security_group_ids = ["sg-0863ae0a342ef1320"]

  depends_on = [
    aws_route53_record.subdomain, aws_route53_record.auth_subdomain
  ]
  
  connection {
    type     = "ssh"
    user     = "ec2-user"
    private_key = file(var.private_key)
    host     = aws_spot_instance_request.wordpress.public_dns
  }

  provisioner "remote-exec" {
    inline = [
      "aws ec2 create-tags --region us-east-1 --resources ${aws_spot_instance_request.wordpress.spot_instance_id} --tags Key=Name,Value=twinsisters-wordpress-test",
      "sudo sed -i 's/SUBDOMAIN/${var.subdomain}/g' certbot.sh",
      "sudo sed -i 's/ENVIRONMENT/${var.environment}/g' certbot.sh",
      "sudo ./certbot.sh .",
      "sudo wp search-replace --path=/usr/share/nginx/html 'http://localhost' 'http://${aws_spot_instance_request.wordpress.public_dns}'",
      "sudo wp option --path=/usr/share/nginx/html update home 'http://${var.subdomain}.appertly.com'",
      "sudo wp option --path=/usr/share/nginx/html update siteurl 'http://${var.subdomain}.appertly.com'",
      "sudo sed -i 's/SERVER_NAME/${var.subdomain}.appertly.com/g' CAC-NGINX.conf",
      "sudo sed -i 's/SERVER_NAME/auth.${var.subdomain}.appertly.com/g' twinsisters-auth.appertly.com.conf",
      "sudo sed -i 's/FASTCGI_PARAM/${var.subdomain}.appertly.com/g' twinsisters-auth.appertly.com.conf",
      "sudo sed -i 's/REDIRECT/${var.subdomain}.appertly.com/g' twinsisters-auth.appertly.com.conf",
      "sudo mv w3-nginx.conf /usr/share/nginx/html/nginx.conf",
      "sudo mv CAC-NGINX.conf /etc/nginx/nginx.conf",
      "sudo mv twinsisters-auth.appertly.com.conf /etc/nginx/twinsisters-auth.appertly.com.conf",
      "sudo chmod 777 /etc/nginx/nginx.conf",
      "sudo chmod 777 /etc/nginx/twinsisters-auth.appertly.com.conf",
      "sudo chmod 777 /usr/share/nginx/html/nginx.conf",

      "sudo systemctl restart nginx"
    ]
  }

}