terraform {
  backend "s3" {
    bucket = "quevera-terraform-state"
    key    = "test/terraform.tfstate"
    region = "us-east-1"
    dynamodb_table = "terraform-lock"
  }
}