# twins-wordpress

Packer Commands
docker run --rm -it -w /app -v $(pwd):/app -v /home/ec2-user/.aws/credentials:/root/.aws/credentials hashicorp/packer build -var-file=variables.json -var-file=secrets.json wordpress.json

Terraform commands

Init
docker run --rm -it -w /app -v $(pwd):/app -v ~/.aws/credentials:/root/.aws/credentials  hashicorp/terraform init

Apply
docker run --rm -it -w /app -v $(pwd):/app -v ~/.aws/credentials:/root/.aws/credentials -v /home/ec2-user/environment/keypairs/terraform.pem:/home/ec2-user/environment/keypairs/terraform.pem  hashicorp/terraform apply

Destroy
docker run --rm -it -w /app -v $(pwd):/app -v ~/.aws/credentials:/root/.aws/credentials -v /home/ec2-user/environment/keypairs/terraform.pem:/home/ec2-user/environment/keypairs/terraform.pem  hashicorp/terraform destroy